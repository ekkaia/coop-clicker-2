export const state = () => ({
	user: null,
	expires: null,
	isAuth: false
});

export const mutations = {
	SET_USER(state, user) {
		state.user = user;
	},
	SET_EXPIRES(state, expires) {
		state.expires = expires;
	},
	SET_ISAUTH(state, isAuth) {
		state.isAuth = isAuth;
	}
};

export const actions = {
	nuxtServerInit({ commit }, { req }) {
		if (req.isAuthenticated()) {
			commit("SET_ISAUTH", req.isAuthenticated());
		}

		if (req.user) {
			commit("SET_USER", req.user);
		}

		if (req.session.cookie._expires) {
			commit("SET_EXPIRES", req.session.cookie._expires);
		}
	},
	loginUser({ commit }, user) {
		commit("SET_USER", user);
		commit("SET_ISAUTH", true);
	},
	logout({ commit }) {
		commit("SET_USER", null);
		commit("SET_EXPIRES", null);
		commit("SET_ISAUTH", false);
	}
};
