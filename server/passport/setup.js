const bcrypt = require("bcryptjs");
const User = require("../models/User");
const passport = require("passport");
const LocalStrategy = require("passport-local");

passport.serializeUser((user, cb) => {
	cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
	User.findById(id, (err, user) => {
		if (err) {
			return cb(err);
		}
		cb(err, user);
	});
});

// Login
passport.use(
	new LocalStrategy((username, password, done) => {
		User.findOne({ username: username },
			async (err, user) => {
				if (err) {
					return done(err);
				}

				if (!user) {
					return done(null, false);
				}

				const match = await bcrypt.compare(password, user.password);

				if (!match) {
					return done(null, false);
				}

				return done(null, user);
			}
		);
	})
);

module.exports = passport;