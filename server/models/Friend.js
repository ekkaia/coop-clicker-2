const mongoose = require("mongoose");

const FriendSchema = new mongoose.Schema({
	requester: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	},
	recipient: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	},
	status: {
		type: String,
		enum: [
			0, // Add friend
			1, // Requested
			2, // Pending
			3 // Friends
		]
	},
}, {
	timestamps: true
});

module.exports = Friend = mongoose.model("Friend", FriendSchema);