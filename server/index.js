if (process.env.NODE_ENV !== "production") {
	require("dotenv").config();
}

const express = require("express");
const app = express();

const http = require("http").createServer(app);
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const helmet = require("helmet");
const {
	body,
	validationResult
} = require("express-validator");

const consola = require("consola");
const {
	Nuxt,
	Builder
} = require("nuxt");

const User = require("./models/User");
const Friend = require("./models/Friend");
const passport = require("./passport/setup");
const ensureAuthenticated = require("./passport/isAuth");

const config = require("../nuxt.config.js");
config.dev = process.env.NODE_ENV !== "production";

async function start() {
	const nuxt = new Nuxt(config);

	const {
		host,
		port
	} = nuxt.options.server;

	await nuxt.ready();

	if (config.dev) {
		const builder = new Builder(nuxt);
		await builder.build();
	}

	app.use(nuxt.render);

	http.listen(port, host);

	consola.ready({
		message: `Server listening on http://${host}:${port}`,
		badge: true
	});

	mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@users-n6qfb.gcp.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`, {
		useNewUrlParser: true,
		useCreateIndex: true,
		useUnifiedTopology: true,
		useFindAndModify: false
	});

	const db = mongoose.connection;
	db.on("error", console.error.bind(console, "connection error:"));
	db.once("open", function () {
		consola.start("Connexion établie avec la base de données");
	});
}

start();

app.use(helmet());
app.disable("x-powered-by");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

app.use(
	session({
		secret: process.env.SECRET,
		name: "sessions",
		resave: false,
		saveUninitialized: true,
		store: new MongoStore({
			mongooseConnection: mongoose.connection,
			collection: "sessions"
		}),
		cookie: {
			expires: new Date(Date.now() + 60 * 60 * 1000)
		}
	})
);

app.use(passport.initialize());
app.use(passport.session());

app.post("/login", passport.authenticate("local"), (req, res) => {
	res.send(req.user);
	consola.info(`${req.user.username} has logged in`)
});

app.post("/register", [
		body("username", "Must be at least 6 chars long")
		.isLength({
			min: 6,
			max: 16
		})
		.custom(async value => {
			const user = await User.findOne({
				username: value.toLowerCase()
			});
			if (user) {
				return Promise.reject("Username already in use");
			}
		})
		.withMessage("Username already exists"),
		body("password", "Must be at least 8 chars long").isLength({
			min: 8,
			max: 32
		})
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.json({
				errors: errors.array()
			});
		}

		const hashedPassword = await bcrypt.hash(req.body.password, 10);

		let newUser = new User({
			username: req.body.username.toLowerCase(),
			password: hashedPassword,
			friends: []
		});

		newUser.save().then(item => {
			res.status(200).send(item);
			consola.success(`${item.username} has been registered`);
		}).catch(err => {
			res.status(400).send(`Error ! ${err}`);
		});
	}
);

app.post("/logout", ensureAuthenticated, (req, res) => {
	consola.success(`${req.user.username} has logout`)
	req.session.destroy();
	res.send(true);
});

app.delete("/user", ensureAuthenticated, (req, res) => {
	User.findOneAndDelete({
		_id: req.user._id
	}).then(user => {
		consola.success(`${user.username} account deleted!`);
		res.send(user);
	}).catch(err => {
		res.status(400).send(`Error ! ${err}`);
	});
});

app.post("/search", ensureAuthenticated, (req, res) => {
	User.find({
		username: {
			$regex: `.*${req.body.wantedUser}.*`,
			$ne: req.user.username
		}
	}, (err, user) => {
		if (err) {
			res.status(400).send(err);
		}

		if (!user) {
			res.status(404).send("Not found");
		}

		console.log(user);
		res.status(200).send(user.map(item => {
			return {
				_id: item._id,
				username: item.username
			}
		}));
	});
});

app.post("/addfriend", ensureAuthenticated, async (req, res) => {
	const user = await Friend.findOneAndUpdate({
		requester: req.user._id,
		recipient: req.body.friend
	}, {
		$set: {
			status: 1 // Requested
		}
	}, {
		upsert: true,
		new: true
	});

	const friend = await Friend.findOneAndUpdate({
		recipient: req.user._id,
		requester: req.body.friend
	}, {
		$set: {
			status: 2 // Pending
		}
	}, {
		upsert: true,
		new: true
	});

	await User.findOneAndUpdate({
		_id: req.user._id
	}, {
		$push: {
			friends: user._id
		}
	}).then(res => {
		consola.success(`Friend request sended! ${res}`);
	});

	await User.findOneAndUpdate({
		_id: req.body.friend
	}, {
		$push: {
			friends: friend._id
		}
	}).then(res => {
		consola.success(`Friend request received! ${res}`);
	});
});

app.get("/friends", async (req, res) => {
	res.send(friends);
})