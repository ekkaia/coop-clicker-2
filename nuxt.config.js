module.exports = {
	mode: "universal",
	head: {
		title: process.env.npm_package_name || "",
		meta: [
			{ charset: "utf-8" },
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1"
			},
			{
				hid: "description",
				name: "description",
				content: process.env.npm_package_description || ""
			}
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
	},
	env: {
		baseUrl: process.env.BASE_URL || "http://localhost:3000"
	},
	loading: { color: "#fff" },
	css: [],
	plugins: [],
	modules: ["@nuxtjs/style-resources", "@nuxtjs/axios"],
	styleResources: {
		scss: ["./assets/style/style.scss"]
	},
	build: {
		extend(config, ctx) {}
	}
};
